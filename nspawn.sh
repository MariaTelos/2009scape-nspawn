#!/bin/bash

# Warning! I made this script for singleplayer & development. For now, do not
# expose this server to the internet.

# The name of your nspawn container.
# Will cause errors if edited.
CONTAINER=2009scape

# The version of Ubuntu you will be using for your container.
CODENAME=focal

# The directories for both the containers & the package cache. Editing these may
# result in catastrophic damage to your host system. To work around this,
# symlink another directory to /opt/nspawn
# Example: sudo ln -s /mnt/hdd/nspawn /opt/nspawn
NSPAWN_DIR=/opt/nspawn/machines/${CONTAINER}
CACHE_DIR=${NSPAWN_DIR}/../../cache/ubuntu-${CODENAME}

# The directory where 2009scape's cache will be on the host machine.
GCACHE_DIR=~/.runite_rs/runescape

# The credentals for your user account inside the container.
NSPAWN_USER=user
NSPAWN_PW=password

# The credentals for your container's database.
DB=global
DB_USER=global
DB_PW=password

GAME_CACHE=/opt/lampp/htdocs/downloads/

ARCH_DEPS='git dialog debootstrap ubuntu-keyring'
UBUNTU_DEPS='git dialog debootstrap'
DEBIAN_DEPS='git dialog debootstrap ubuntu-keyring'

# Dependancies for the VM. Gradle & XAMMP are pulled in with wget.
CONTAINER_DEPS=systemd-container
BUILD_DEPS='dialog screen git wget mc htop nano vim zip net-tools'
JAVA_DEPS='openjdk-8-jdk'

# Temp files
TEMP=~/tmp_2009



# This script should work with any Linux distro that uses systemd.
# It may also work with Windows 10 via Linux File System Windows, but I have yet
# to test that.
# Add any other OS-dependant functions below this one.
arch_install(){
	sudo pacman -Syu ${ARCH_DEPS}
}

ubuntu_install(){
	sudo apt-update; sudo apt upgrade
	sudo apt install ${UBUNTU_DEPS} -y
}

debian_install(){
	sudo apt-update; sudo apt upgrade
	sudo apt install ${DEBIAN_DEPS} -y
}

host_vm_init(){
	echo
	echo
	echo "Creating directories for the VM & Apt cache."
	echo
	echo ; sleep 3
	sudo mkdir -p ${NSPAWN_DIR}
	sudo mkdir -p ${NSPAWN_DIR}/../../cache/ubuntu-${CODENAME}
	sudo chmod 700 ${NSPAWN_DIR}/../../machines
}



host_install_vm(){
	if [[ -d ${NSPAWN_DIR}/root ]]; then
		echo "Virtual machine already exists, aborting."
	else
		echo
		echo
		echo "Now installing Ubuntu-${CODENAME}."
		echo
		echo ; sleep 3
		sudo debootstrap --cache-dir=${CACHE_DIR} --include=${CONTAINER_DEPS} --components=main,universe ${CODENAME} ${NSPAWN_DIR} http://archive.ubuntu.com/ubuntu/
		sudo rm ${NSPAWN_DIR}/etc/hostname
		sudo echo "127.0.0.1 localhost" > ${NSPAWN_DIR}/etc/hosts
		sudo echo "127.0.1.1 ${CONTAINER}" >> ${NSPAWN_DIR}/etc/hosts
		sudo rm ${NSPAWN_DIR}/etc/resolv.conf
		sudo mkdir -p ${NSPAWN_DIR}/etc/skel/screens
		sudo chmod 700 ${NSPAWN_DIR}/etc/skel/screens
		sudo echo "Dir::Cache::Archives /opt/nspawn/cache/ubuntu-${CODENAME};" >> ${NSPAWN_DIR}/etc/apt/apt.conf
		sudo echo "Binary::apt::APT::Keep-Downloaded-Packages true;" >> ${NSPAWN_DIR}/etc/apt/apt.conf
	fi
}

host_install_services(){
	echo
	echo
	echo "Installing service files for ${CONTAINER}."
	echo
	echo ; sleep 3
	sudo mkdir -p /etc/systemd/nspawn
	sudo cp -f host-files/${CONTAINER}.nspawn /etc/systemd/nspawn/
	sudo cp -f host-files/systemd-nspawn@${CONTAINER}.service /etc/systemd/system/
	sudo ln -rsf ${NSPAWN_DIR} /var/lib/machines/${CONTAINER}
	sudo machinectl start ${CONTAINER}
}


vm_make_user(){
	echo
	echo
	echo "Creating user for VM."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		useradd -m -s /bin/bash '${NSPAWN_USER}'
	'
}




vm_install_gradle() {
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ ! -f "gradle-6.6.1/bin/gradle" ]]; then
			echo
			echo
			echo "Gradle not found, checking if the installer exists"
			echo
			echo ; sleep 3
			if [[ -f gradle-6.6.1-bin.zip ]]; then
				echo
				echo
				echo "Gradle installer found, unzipping."
				echo
				echo  ; sleep 3
				unzip gradle-6.6.1-bin.zip
			else
				echo
				echo
				echo "Gradle installer not found, downloading & unzipping."
				echo
				echo ; sleep 3
				wget https://services.gradle.org/distributions/gradle-6.6.1-bin.zip
				unzip gradle-6.6.1-bin.zip
			fi
		else
			echo
			echo
			echo "Gradle-6.6.1 already installed."
			echo
			echo ; sleep 3
		fi
	'
}

vm_install_xampp() {
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -f "/opt/lampp/lampp" ]]; then
			echo
			echo
			echo "XAMMP already installed."
			echo
			echo ; sleep 3
		else
			if [[ -f "xampp-linux-x64-7.2.34-0-installer.run" ]]; then
				echo
				echo
				echo "XAMPP already downloaded, skipping download."
				echo
				echo ; sleep 3
			else
				echo
				echo
				echo "XAMMP installer not found, now downloading."
				echo
				echo ; sleep 3
				wget https://www.apachefriends.org/xampp-files/7.2.34/xampp-linux-x64-7.2.34-0-installer.run
				chmod 700 xampp-linux-x64-7.2.34-0-installer.run
			fi
		fi
	'
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		if [[ -f /opt/lampp/lampp ]]; then
			echo
			echo
			echo "XAMMP already installed."
			echo
			echo ; sleep 3
		else
			echo
			echo
			echo "Now installing XAMMP. This may take a while."
			echo
			echo ; sleep 3
			if [[ -f /home/user/xampp-linux-x64-7.2.34-0-installer.run ]]; then
				cd /home/user
				./xampp-linux-x64-7.2.34-0-installer.run  --mode unattended
				/opt/lampp/lampp stop
				/opt/lampp/lampp start
				/opt/lampp/bin/mysql.server start
				mkdir -p '${GAME_CACHE}'cache
			fi
		fi
	'
}




vm_clone_2009_dev() {
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -d "2009Scape" ]]; then
			echo
			echo
			echo "2009Scape work directory already exists, skipping git clone."
			echo
			echo ; sleep 3
		else
			echo
			echo
			echo "2009Scape work directory not found, shallow cloning development branch."
			echo
			echo ; sleep 3
			git clone --depth=1 --branch=development https://github.com/2009scape/2009Scape.git
		fi
	'
}

# The firewall & fail2ban be added later once I start isolating the container
# from the host system.

vm_install_deps(){
	echo
	echo
	echo "Installing 2009scape dependencies."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		apt update ; apt upgrade
		apt install '"${BUILD_DEPS} ${JAVA_DEPS}"' -y
	'
}

vm_db_init(){
	echo
	echo
	echo "Setting up MariaDB & installing global.sql"
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		if [[ -f "/home/user/db.failsafe" ]]; then
			echo
			echo
			echo "Failsafe detected, skipping database initialization."
			echo
			echo ; sleep 3
		else
			cd /home/user/2009Scape/Server/db_exports
			/opt/lampp/bin/mysql -e "CREATE USER '${DB_USER}' IDENTIFIED BY '"'${DB_PW}'"';"
			/opt/lampp/bin/mysql -e "CREATE DATABASE '${DB}';"
			/opt/lampp/bin/mysql -e "GRANT ALL privileges ON '${DB}'.* TO '${DB_USER}'@localhost IDENTIFIED BY '"'${DB_PW}'"';"
			gunzip global.sql.gz
			/opt/lampp/bin/mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < global.sql
			touch /home/user/db.failsafe
			chown user:user /home/user/db.failsafe
		fi
	'
}

vm_build_game_server(){
	echo
	echo
	echo "Compiling 2009Scape Game server."
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -f gradle-6.6.1/bin/gradle ]]; then
			cd ~/2009Scape/Server
			../../gradle-6.6.1/bin/gradle build
			cd build/libs
			cp -r ../../data data
			cp -r ../../scripts scripts
			cp -r ../../worldprops worldprops
		else
			echo
			echo
			echo "Gradle-6.6.1 not installed."
			echo
			echo ; sleep 3
		fi
	'

	echo
	echo
	echo "Copying game cache to htdocs."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		if [[ ! -d /home/user/2009Scape ]]; then
			echo
			echo
			echo "2009Scape work directory not found."
			echo
			echo ; sleep 3
		else
			if [[ ! -f /home/user/2009Scape/Server/data/cache/main_file_cache.dat2 ]]; then
				echo "2009Scape cache missing, aborting."
			else
				cd /home/user/2009Scape/Server/data/cache
				zip '${GAME_CACHE}'530file_store_.zip *
			fi
		fi
	'
}

vm_build_man_server(){
	echo
	echo
	echo "Compiling 2009Scape Management server."
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -f "gradle-6.6.1/bin/gradle" ]]; then
			cd ~/2009Scape/Management-Server
			../../gradle-6.6.1/bin/gradle build
			cd build/distributions
			tar -xvf Management-Server-1.0.0.tar
		else
			echo
			echo
			echo "Gradle-6.6.1 not installed, unable to compile Management Server."
			echo
			echo ; sleep 3
		fi
	'
}

vm_build_singleplayer(){
	echo
	echo
	echo "Compiling localhost client."
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -f "gradle-6.6.1/bin/gradle" ]]; then
			cd ~/2009Scape/Client
			echo target_ip_addr:127.0.0.1 > src/main/resources/client.conf
			../../gradle-6.6.1/bin/gradle build
			cd build/distributions
			tar -xvf Client-1.0.0.tar
		else
			echo
			echo
			echo "Gradle-6.6.1 not installed, unable to compile localhost client."
			echo
			echo ; sleep 3
		fi
	'

	echo
	echo
	echo "Copying localhost client to htdocs."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		if [[ ! -d /home/user/2009Scape ]]; then
			echo
			echo
			echo "2009Scape work directory not found."
			echo
			echo ; sleep 3
		else
			if [[ ! -f /home/user/2009Scape/Client/build/distributions/Client-1.0.0.zip ]]; then
				echo "Client-1.0.0.zip missing." ; sleep 3
			else
				cd /home/user/2009Scape/Client/build/distributions
				cp -f Client-1.0.0.zip '${GAME_CACHE}'Client-local-1.0.0.zip
				cd ../libs
				cp -f client-1.0.0.jar '${GAME_CACHE}'client-local-1.0.0.jar
				cp -f client-1.0.0-all.jar '${GAME_CACHE}'client-local-1.0.0-all.jar
			fi
		fi
	'
}

vm_build_official(){
	echo
	echo
	echo "Compiling official 2009Scape client."
	echo
	echo
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		if [[ -f "gradle-6.6.1/bin/gradle" ]]; then
			cd ~/2009Scape/Client
			echo target_ip_addr:game.2009scape.com > src/main/resources/client.conf
			../../gradle-6.6.1/bin/gradle build
			cd build/distributions
			tar -xvf Client-1.0.0.tar
		else
			echo
			echo
			echo "Gradle-6.6.1 not installed."
			echo
			echo ; sleep 3
		fi
	'

	echo
	echo
	echo "Copying official 2009scape client to htdocs."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		if [[ ! -d "/home/user/2009Scape" ]]; then
			echo
			echo
			echo "2009Scape work directory not found, aborting."
			echo
			echo ; sleep 3
		else
			if [[ ! -f /home/user/2009Scape/Client/build/distributions/Client-1.0.0.zip ]]; then
				echo "Client-1.0.0.zip missing." ; sleep 3
			else
				cd /home/user/2009Scape/Client/build/distributions
				cp -f Client-1.0.0.zip '${GAME_CACHE}'2009Scape.zip
				cd ../libs
				cp -f client-1.0.0.jar '${GAME_CACHE}'2009Scape.jar
				cp -f client-1.0.0-all.jar '${GAME_CACHE}'2009Scape.jar
			fi
		fi
	'
}

start_man_server(){
	echo
	echo
	echo "Starting Management Server at ~/screens"
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		cd ~/2009Scape/Management-Server/build/libs
		SCREENDIR=/home/user/screens screen -dmS man-server java -jar managementserver-1.0.0.jar
		SCREENDIR=/home/user/screens screen -list
	'
}

start_game_Server(){
	echo
	echo
	echo "Starting Game Server at ~/screens"
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		cd ~/2009Scape/Server/build/libs/
		SCREENDIR=/home/user/screens screen -dmS game-server java -jar server-1.0.0.jar
		SCREENDIR=/home/user/screens screen -list
	'
}

vm_wipe_screens() {
	echo
	echo
	echo "Removing dead screens at ~/screens"
	echo
	echo ; sleep 3
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		SCREENDIR=/home/user/screens screen -wipe
	'
}

screen_man_server(){
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		dialog --msgbox "Press CTRL + A + D to detach from the session." 5 50
		SCREENDIR=/home/user/screens screen -r man-server
	'
}

screen_game_server(){
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
		dialog --msgbox "Press CTRL + A + D to detach from the session." 5 50
		SCREENDIR=/home/user/screens screen -r game-server
	'
}

# Executing just "lammp start" fails to start MariaDB despite what the output says.
start_lampp(){
	echo
	echo
	echo "Starting XAMPP server."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		/opt/lampp/lampp start
		/opt/lampp/bin/mysql.server start
	'
}

stop_lampp(){
	echo
	echo
	echo "Stopping XAMPP server."
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		/opt/lampp/lampp stop
	'
}

vm_shell_root(){
	sudo machinectl shell root@${CONTAINER}
}

vm_shell_user(){
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER}
}

vm_mc_root(){
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
	mc /home/user
	'
}

vm_mc_user(){
	sudo machinectl shell ${NSPAWN_USER}@${CONTAINER} /bin/bash -c '
	mc ~/
	'
}

vm_power_on() {
	echo
	echo
	echo "Starting up ${CONTAINER}-vm"
	echo
	echo ; sleep 3
	sudo machinectl start ${CONTAINER}
}

vm_power_off() {
	echo
	echo
	echo "Shutting down ${CONTAINER}-vm"
	echo
	echo ; sleep 3
	sudo machinectl shell root@${CONTAINER} /bin/bash -c '
		shutdown -h now
	'
}

vm_one_click() {
	if [[ -d ${NSPAWN_DIR}/root ]]; then
		echo
		echo
		echo "Install directory not empty."
		echo
		echo ; sleep 3
		main_menu
	else
		host_vm_init; host_install_vm; host_install_services; vm_make_user; vm_install_deps; vm_install_xampp; vm_install_gradle; vm_clone_2009_dev; vm_db_init; vm_build_singleplayer; vm_build_official; vm_build_man_server; vm_build_game_server; sleep 5; main_menu
	fi
}

shell_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "2009Scape Shell Menu" --menu "Server must be powered on." 0 50 0 \
		"1" "Midnight Commander (root)" \
		"2" "Midnight commander (user)" \
		"3" "/bin/bash (root)" \
		"4" "/bin/bash (user)" \
		"5" "Main Menu" 2>${TEMP}
	case $(cat ${TEMP}) in
		"1") clear; vm_mc_root; shell_menu
			;;
		"2") clear; vm_mc_user; shell_menu
			;;
		"3") clear; vm_shell_root; shell_menu
			;;
		"4") clear; vm_shell_user; shell_menu
			;;
		"5") main_menu
	esac
}

compile_menu(){
	dialog  --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "Welcome to Open 2009Scape"  --menu "What would you like to do?" 0 40 0\
			"1" "Compile localhost client" \
			"2" "Compile official client" \
			"3" "Compile Management Server" \
			"4" "Compile Game Server" \
			"5" "Compile all" \
			"6" "Exit" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) clear; vm_build_singleplayer; sleep 5 ; compile_menu
			;;
		"2" ) clear; vm_build_official; sleep 5 ; compile_menu
			;;
		"3" ) clear; vm_build_man_server ; sleep 5 ; compile_menu
			;;
		"4" ) clear; vm_build_game_server ; sleep 5 ; compile_menu
			;;
		"5" ) clear; vm_build_singleplayer ; vm_build_official ; vm_build_man_server ; vm_build_game_server ; sleep 5 ; main_menu
			;;
		"6" ) main_menu
	esac
}

os_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "2009Scape Install Menu" --menu "\
		Which distro are you running?" 0 40 0 \
			"1" "Arch Linux" \
			"2" "Ubuntu" \
			"3" "Debian" \
			"4" "Install Menu" 2>${TEMP}

		case $(cat ${TEMP}) in
			"1" ) clear; arch_install; sleep 5; install_menu
				;;
			"2" ) clear; ubuntu_install; sleep 5; install_menu
				;;
			"3" ) clear; debian_install; sleep 5; install_menu
				;;
			"4" ) install_menu
		esac
}

install_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "2009Scape Install Menu" --menu "\
		The first two steps require you to run this script as root or sudo, but you wont need a password for the rest \
		of the script if you follow the examples listed inside the provided sudoers file." 10 60 0 \
			"1" "Install host system dependencies" \
			"2" "Install container" \
			"3" "Setup user account(s) for the container" \
			"4" "Install ${CONTAINER} dependencies" \
			"5" "Setup Database" \
			"6" "Compile 2009Scape client & servers" \
			"7" "Main Menu" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) os_menu
			;;
		"2" ) clear; host_vm_init; sleep 5 ; host_install_vm; sleep 5 ; host_install_services ; sleep 5 ; install_menu
			;;
		"3" ) clear; vm_make_user; sleep 5; install_menu
			;;
		"4" ) clear; vm_install_deps; sleep 5 ; vm_install_xampp ; sleep 5 ; vm_install_gradle ; vm_clone_2009_dev ; sleep 5 ; install_menu
			;;
		"5" ) clear ; vm_db_init; sleep 5; install_menu
			;;
		"6" ) clear ; vm_build_singleplayer; sleep 5; vm_build_official ; sleep 5 ; vm_build_man_server ; sleep 5; vm_build_game_server ; sleep 5; main_menu
			;;
		"7" ) main_menu
	esac
}

server_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "2009Scape Server menu." --menu "Client and cache are located at http://127.0.0.1/downloads. Start XAMMP server before starting 2009scape.." 0 50 0 \
		"1" "Start Management Server" \
		"2" "Start Game Server" \
		"3" "View Management Server" \
		"4" "View Game Server" \
		"5" "Wipe inactive sessions." \
		"6" "Start XAMPP" \
		"7" "Stop XAMPP" \
		"8" "Main Menu" 2>${TEMP}
	case $(cat ${TEMP}) in
		"1" ) clear; start_man_server ; sleep 5 ; server_menu
			;;
		"2" ) clear; start_game_Server ; sleep 5 ; server_menu
			;;
		"3" ) clear; screen_man_server;  server_menu
			;;
		"4" ) clear; screen_game_server ;  server_menu
			;;
		"5" ) clear; vm_wipe_screens ; sleep 3 ; server_menu
			;;
		"6" ) clear ; start_lampp ; sleep 5 ; server_menu
			;;
		"7" ) clear ; stop_lampp ; sleep 5 ; server_menu
			;;
		"8" ) main_menu
	esac
}

power_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "Welcome to 2009Scape" --menu "Power Menu" 0 40 0 \
		"1" "Power on ${CONTAINER}" \
		"2" "Reboot ${CONTAINER}" \
		"3" "Shutdown ${CONTAINER}" \
		"4"  "Main Menu" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) clear; vm_power_on ; power_menu
			;;
		"2" ) clear; vm_power_off ; vm_power_on ; power_menu
			;;
		"3" ) clear; vm_power_off ; power_menu
			;;
		"4" ) main_menu
	esac
}

main_menu(){
	rm ${TEMP}
	touch ${TEMP}
	dialog  --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})" --title "Welcome to Open 2009Scape"  --menu "What would you like to do?" 0 40 0\
	    "1" "Power Menu" \
	    "2" "Server Menu" \
	    "3" "Shell Menu" \
	    "4" "Install 2009Scape" \
			"5" "Install 2009Scale (one-click)" \
			"6" "Compile Menu" \
	    "7" "Exit" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) power_menu
			;;
		"2" ) server_menu
			;;
		"3" ) shell_menu
			;;
		"4" ) install_menu
			;;
		"5" ) clear; vm_one_click
			;;
		"6" ) compile_menu
			;;
		"7" ) rm ${TEMP} ; clear ; exit
	esac
}

#while true
#	do
		main_menu
#	done
